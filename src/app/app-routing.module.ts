import { TasksComponent } from './tasks/tasks/tasks.component';
import { AuthGuard } from './_helpers/auth.guard';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', component: DashboardComponent, canActivate: [AuthGuard],


  },
  {
    path: 'tasks', component: TasksComponent, canActivate: [AuthGuard]
  },

  {

    path: 'auth',
    loadChildren: () => import(`./authentication/authentication.module`).then(
      module => module.AuthenticationModule
    )
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
