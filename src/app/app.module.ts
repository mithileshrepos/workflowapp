import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgDragDropModule } from 'ng-drag-drop';


import { SharedModule } from './shared/shared.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { ModalComponentComponent } from './modal-component/modal-component.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskmodalComponent } from './taskmodal/taskmodal.component';
import { CustomdatePipe } from './pipes/customdate.pipe';
import { TasksComponent } from './tasks/tasks/tasks.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ModalComponentComponent,
    TaskmodalComponent,
    CustomdatePipe,
    TasksComponent,


  ],
  entryComponents: [
    ModalComponentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgDragDropModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
