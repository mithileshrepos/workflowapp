import { NavbarService } from './../../../services/navbar.service';
import { LoginService } from './../../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signinForm: FormGroup;
  loginDetails: Object = {};
  return: string = '';

  constructor(private loginservice: LoginService, private router: Router,
    private route: ActivatedRoute, private navbarService: NavbarService) { }

  ngOnInit(): void {

    this.signinForm = this.createFormGroup();
    this.route.queryParams
      .subscribe(params => this.return = params['return'] || '/');
  }

  createFormGroup() {
    return new FormGroup({
      username: new FormControl('', Validators.compose([
        Validators.minLength(3),
        Validators.required])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(8),
        Validators.required]))
    })





  }


  onLogin() {
    this.loginDetails = this.signinForm.value;

    this.loginservice.loginUser(this.loginDetails);


    this.navbarService.updateNavAfterAuth('user');
    this.navbarService.updateLoginStatus(true);
    //this.role = 'user';

    this.router.navigateByUrl(this.return);

    this.signinForm.reset();

  }



}
