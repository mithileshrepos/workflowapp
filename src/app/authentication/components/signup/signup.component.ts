import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  user: Object;

  constructor(private userservice: UserService) { }

  ngOnInit(): void {
    this.signupForm = this.createFormGroup();
  }

  createFormGroup() {
    return new FormGroup({
      username: new FormControl('', Validators.compose([
        Validators.minLength(3),
        Validators.required])),
      email: new FormControl('', Validators.compose([
        Validators.email,
        Validators.required])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(8),
        Validators.required]))
    })


  }
  onSubmit() {
    console.log(this.signupForm.value.email)

    this.user = this.signupForm.value;
    this.user['isOwner'] = false;
    this.userservice.saveUser(this.user)
    this.signupForm.reset();


  }
}
