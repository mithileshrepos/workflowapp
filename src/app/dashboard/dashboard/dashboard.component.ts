import { DataService } from './../../services/data.service';
import { ModalComponentComponent } from './../../modal-component/modal-component.component';
import { ModalService } from './../../services/modal.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Component, OnInit, OnChanges } from '@angular/core';
import { Task } from '../../models/Task';
import { TaskmodalComponent } from '../../taskmodal/taskmodal.component';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  tasks: Task[] = [];

  constructor(private router: Router, private modalService: NgbModal, private data: DataService, private ModalService: ModalService) {


  }




  ngOnInit(): void {
    this.data.currentMessage.subscribe(tasks => {
      this.tasks = tasks

      this.tasks = JSON.parse(sessionStorage.getItem("tasks")) || [];
    })
    console.log("using service", this.tasks);

  }

  deleteTask(t) {
    console.log("to be deleted ", t)
    const filterObjects = (key, value) => {

      return this.tasks.filter((e) => {

        if (e && e.hasOwnProperty(key) && e[key] === value) {
          return false;
        }

        return true;
      });
    };

    this.tasks = filterObjects("title", t.title)
    sessionStorage.setItem("tasks", JSON.stringify(this.tasks));
  }


  openModal(link) {
    const modalRef = this.modalService.open(ModalComponentComponent);
    modalRef.componentInstance.src = link;
  }

  openTaskModal(link, t) {
    this.ModalService.clickTask(t);
    const modalRef = this.modalService.open(TaskmodalComponent);
    modalRef.componentInstance.src = link;
  }

}