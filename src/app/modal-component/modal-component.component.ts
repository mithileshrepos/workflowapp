import { DataService } from './../services/data.service';
import { ModalService } from './../services/modal.service';
import { Task } from './../models/Task';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-modal-component',
  templateUrl: './modal-component.component.html',
  styleUrls: ['./modal-component.component.css']
})
export class ModalComponentComponent implements OnInit {

  myFiles: string[] = [];


  tasks: any[] = [];
  task: Task;

  myForm: FormGroup;

  constructor(public activeModal: NgbActiveModal, private data: DataService) {

  }

  ngOnInit(): void {

    this.data.currentMessage.subscribe(tasks => this.tasks = tasks)

    this.myForm = new FormGroup({
      title: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
      description: new FormControl('', [Validators.required, Validators.minLength(3)]),
      file: new FormControl('', [Validators.required]),
      owners: new FormControl('', [Validators.required])
    });

  }

  get f() {
    return this.myForm.controls;
  }

  onFileChange(event) {

    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
  }

  submit() {
    const formData = new FormData();


    this.myForm.value['attachments'] = this.myFiles.length;
    this.myForm.value['state'] = 'TODO'
    this.myForm.value['date'] = new Date();
    this.task = this.myForm.value;

    this.tasks = JSON.parse(sessionStorage.getItem("tasks")) || [];
    this.tasks.push(this.task);

    sessionStorage.setItem("tasks", JSON.stringify(this.tasks));

    console.log(this.tasks);
    this.data.changeMessage(this.tasks);

    for (var i = 0; i < this.myFiles.length; i++) {
      formData.append("file[]", this.myFiles[i]);
    }
    console.log(this.myFiles);
    this.myForm.reset();

    /*this.http.post('http://localhost:8001/upload.php', formData)
      .subscribe(res => {
        console.log(res);
        alert('Uploaded Successfully.');
      }) */
  }

}
