export class Task {
    title: string = '';
    description: string = '';
    attachements: number;
    owners: string[];
    state: string;
    date: Date = new Date();

}