import { Task } from './../models/Task';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private messageSource = new BehaviorSubject<Task[]>([]);
  currentMessage = this.messageSource.asObservable();

  constructor() { }


  changeMessage(message: Task[]) {
    this.messageSource.next(message)
  }

}
