import { User } from './../models/User';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  users: User[] = [{
    "username": "mithilesh",
    "email": "mipra282@gmail.com",
    "password": "12345678",
    "isAdmin": true
  },
  {
    "username": "mithilesh12",
    "email": "mip@gmail.com",
    "password": "12345678",
    "isAdmin": false
  }];



  isUserLoggedIn: Boolean;

  constructor(private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }


  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }



  loginUser(userLoginDetails) {

    this.isUserLoggedIn = false;

    this.users.map(user => {

      if ((user['username'] == userLoginDetails['username']) && (user['password'] == user['password'])) {

        this.isUserLoggedIn = true;
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;

      }

    })


    console.log(this.isUserLoggedIn);





  }

  logoutUser() {

    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.isUserLoggedIn = false;

  }
}
