import { LoginService } from './../../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarService } from '../../../services/navbar.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  links: Array<{ text: string, path: string }>;
  isLoggedIn = false;


  constructor(private router: Router, private navbarService: NavbarService, private LoginService: LoginService) {
    if (localStorage.getItem('currentUser')) {
      this.isLoggedIn = true;
    }
  }

  ngOnInit(): void {

    this.links = this.navbarService.getLinks();
    this.navbarService.getLoginStatus().subscribe(status => this.isLoggedIn = status);
  }

  logout() {
    this.navbarService.updateLoginStatus(false);
    this.router.navigate(['/auth/login']);
    this.LoginService.logoutUser();
  }

}




