import { ModalService } from './../services/modal.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-taskmodal',
  templateUrl: './taskmodal.component.html',
  styleUrls: ['./taskmodal.component.css']
})
export class TaskmodalComponent implements OnInit {

  clickedTask: any;
  constructor(public activeModal: NgbActiveModal, private modalService: ModalService) { }

  ngOnInit(): void {
    this.clickedTask = this.modalService.clickedTask;
    console.log(this.clickedTask);
  }

}
