import { Task } from './../../models/Task';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasks: Task[];

  constructor() { }

  ngOnInit(): void {

    this.tasks = JSON.parse(sessionStorage.getItem("tasks"));
  }

}
